head = Array.new

ARGV.each do |argv|
input = argv
  file_name = input.sub(".csv", "").sub(/^.+\//, "").sub(/_.+/, "")
  f = File.open(input, "r+:Shift_JIS")

  flag = 0
  f.each do |line|

    words = line.sub("\r\n", "").split(",")

    if flag == 1
      i = 0
      printf("@%s = %s.new\n", file_name, file_name.capitalize)
      words.each do |word|
        if head[i] == "id"
          i += 1
          next
        end
        if head[i] == "category_id"
            printf("@%s.%s = %s\n", file_name, head[i], word.encode("utf-8"))
        else
            printf("@%s.%s = '%s'\n", file_name, head[i], word.encode("utf-8"))
        end
        i += 1
      end
      printf("@%s.save\n\n", file_name)
    end

    if flag == 0
      flag = 1
      head = words
    end
  end
  f.close
end

